import time
import uuid

import stomp


class MsgListener(stomp.ConnectionListener):

    def on_error(self, message):
        print(f'received an error {message.body}')
        with open(f'output/ERROR-{time.monotonic_ns()}-{uuid.uuid4()}', 'w') as f:
            f.write(message.body)

    def on_message(self, frame):
        print(f'received a message {frame.body}')
        with open(f'output/{time.monotonic_ns()}-{uuid.uuid4()}', 'w') as f:
            f.write(frame.body)

def main() -> None:
    conn: stomp.Connection = stomp.Connection()
    hosts = [('lib-hydratail-staging', 61613)]

    # Create a connection object by passing the hosts as an argument
    conn: stomp.Connection = stomp.Connection(host_and_ports=hosts)

    # Tell the connection object to listen for messages using the
    # Listener class we created above
    listener: MsgListener = MsgListener()
    conn.set_listener('msg', listener)

    conn.connect('admin', 'admin', wait=True)
    conn.subscribe('/queue/Consumer.tap.VirtualTopic.dams', id=123, ack='auto')

    time.sleep(2) # If it doesn't sleep, it doesn't seem to work.  I know the feeling.
    conn.unsubscribe(123)
    conn.disconnect()

if __name__ == '__main__':
    main()