TL;DR

```
./build.sh
./confsetup.sh

At '#' prompt:

chown activemq:activemq /mnt/conf
chown activemq:activemq /mnt/data
cp -a /opt/activemq/conf/* /mnt/conf/
cp -a /opt/activemq/data/* /mnt/data/
exit

./mqup.sh

http://localhost:8161
username is admin
password is admin

Click on Topic

Create VirtualTopic.dev

Click Queue

Create Consumer.dev.VirtualTopic.dev

To post:

curl -u admin:admin -d "body=message" http://localhost:8161/api/message/VirtualTopic.dev?type=topic

To read:

wget --user admin --password admin --save-cookies cookies.txt --load-cookies cookies.txt --keep-session-cookies  http://localhost:8161/api/message/Consumer.dev.VirtualTopic.dev?type=queue

```
