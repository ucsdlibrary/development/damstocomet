from pathlib import Path

from rich import print as rprint


def process_file(file: Path) -> None:
    with open(file) as f:
        msg: str = f.readline()
        data: str = msg.split(':')[-1].strip()
        if len(data.split(' ')) == 2:
            ark: str = data.split(' ')[0].strip()
            action: str = data.split(' ')[1].strip()
            if len(ark) == 10:
                # print(f'\tARK: {ark}')
                rprint(f'\tARK Path: /localStore/{ark[0:2]}/{ark[2:4]}/{ark[4:6]}/{ark[6:8]}/{ark[8:]}/')
            else:
                print(f'ARK is of unexpected length: {ark} ({len(ark)})')

            print(f'\tAction: {action.strip("(").strip(")")}')


files =  Path('./output').glob('*')

for file in files:
    print(file)
    process_file(file)
