#!/bin/bash
docker run --platform=linux/amd64 --user root --rm -ti \
  -v $(pwd)/conf:/mnt/conf \
  -v $(pwd)/data:/mnt/data \
  rmohr/activemq:5.14.0-alpine /bin/sh
