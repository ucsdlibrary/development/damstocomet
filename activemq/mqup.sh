#!/bin/bash
docker run \
        -v $(pwd)/conf:/opt/activemq/conf \
        -v $(pwd)/data:/opt/activemq/data \
	-p 8161:8161 -p 61613:61613 -p 61616:61616 activemq
