# This file can't be run "as is" since there are some shell commands that need to be run in it.
# It's here for convenience/reference

docker build --platform linux/amd64 -t activemq .

$CurDir = Get-Location
docker run --platform=linux/amd64 --user root --rm -ti -v $CurDir/conf:/mnt/conf -v $CurDir/data:/mnt/data rmohr/activemq:5.14.0-alpine /bin/sh

# This needs to be run inside the shell
chown activemq:activemq /mnt/conf
chown activemq:activemq /mnt/data
cp -a /opt/activemq/conf/* /mnt/conf/
cp -a /opt/activemq/data/* /mnt/data/
exit

docker run -v $CurDir/conf:/opt/activemq/conf -v $CurDir/data:/opt/activemq/data -p 8161:8161 -p 61613:61613 -p 61616:61616 activemq